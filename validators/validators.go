package validators

import (
	"strings"

	"github.com/go-playground/validator/v10"
)

// https://github.com/go-playground/validator/tree/master/_examples

func ValidateCoolTitle(field validator.FieldLevel) bool {
	return strings.Contains(field.Field().String(), "Cool")
}
