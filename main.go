package main

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	gindump "github.com/tpkeeper/gin-dump"
	"main.go/controllers"
	"main.go/entity/service"
	"main.go/middlewares"
	"main.go/repository"
)

var (
	videoRepository repository.VideoRepository  = repository.NewVideoRepository()
	videoServide    service.VideoService        = service.New(videoRepository)
	videoController controllers.VideoController = controllers.New(videoServide)
)

func setupLogOutput() {
	f, _ := os.Create("ginLog.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	defer videoRepository.CloseDB()
	setupLogOutput()
	gin.SetMode(gin.DebugMode)
	router := gin.New()
	router.Static("css", "./templates/css")
	router.Static("js", "./templates/js")
	router.LoadHTMLGlob("templates/*.html")
	router.Use(gin.Recovery(), middlewares.Logger(), middlewares.BasicAuth(), gindump.Dump())

	apiRoutes := router.Group("/api")
	{
		apiRoutes.GET("/videos", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})
		apiRoutes.POST("/videos", func(ctx *gin.Context) {
			err := videoController.Save(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"message": "Video input is valid!!!!!"})
			}
		})
		apiRoutes.PUT("/videos/:id", func(ctx *gin.Context) {
			err := videoController.Update(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"message": "Video input is valid!!!!!"})
			}
		})
		apiRoutes.DELETE("/videos/:id", func(ctx *gin.Context) {
			err := videoController.Delete(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			} else {
				ctx.JSON(http.StatusOK, gin.H{"message": "Video input is valid!!!!!"})
			}
		})
	}
	viewRoutes := router.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)

	}

	fmt.Println("Running!!!!!!!!!!!!!!!!")

	router.Run(":8080")

}
