package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func tester() {

	url := "http://localhost:8080/videos"
	method := "GET"

	payload := strings.NewReader(`{
    "title": "Lilit Hovhannisyan",
    "description": "Some desc Lilit Hovhannisyan",
    "url": "https://www.youtube.com/watch?v=BXFfw0yCBa0"
}`)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("Authorization", "Basic cHJvZ21hdGljOnJldmlld3M=")
	req.Header.Add("Content-Type", "text/plain")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(body))
}
