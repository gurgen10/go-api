package service

import (
	"main.go/entity"
	"main.go/repository"
)

type VideoService interface {
	Save(entity.Video) entity.Video
	FindAll() []entity.Video
	Update(video entity.Video)
	Delete(video entity.Video)
}

type videoService struct {
	videoRepository repository.VideoRepository
}

func New(repo repository.VideoRepository) VideoService {
	return &videoService{
		videoRepository: repo,
	}
}

func (s *videoService) FindAll() []entity.Video {
	return s.videoRepository.FindAll()
}

func (s *videoService) Save(video entity.Video) entity.Video {
	s.videoRepository.Save(video)
	return video

}
func (s *videoService) Update(video entity.Video) {
	s.videoRepository.Update(video)
}
func (s *videoService) Delete(video entity.Video) {
	s.videoRepository.Delete(video)
}
